.PHONY: all depend precommit unit functional build login publish

ifndef CI
  CONTAINER_ENGINE := podman
  IMAGE_NAME := images_testing
  IMAGE_TAG := latest
endif

ifeq "$(ENV)" "CI"
  CONTAINER_ENGINE := docker
  IMAGE_NAME := ${CI_REGISTRY_IMAGE}
  IMAGE_TAG := ${CI_COMMIT_REF_SLUG}
endif

ifeq "$(ENV)" "PROD"
  CONTAINER_ENGINE := docker
  IMAGE_NAME := ${CI_REGISTRY_IMAGE}
  IMAGE_TAG := latest
endif

all: depend precommit unit build functional

depend:
	curl -sSL https://install.python-poetry.org | python3 - --version 1.2.1
	poetry install --with "$a"

precommit:
	poetry run pre-commit run --all-files

unit:
	poetry run pytest --cov=python_template --cov-report=html --cov-report=xml \
	--cov-report=term-missing --cov-branch --junitxml=junit.xml \
	--cov-fail-under=100 -v tests/unit

functional:
	poetry run pytest tests/functional

build:
	${CONTAINER_ENGINE} build -t ${IMAGE_NAME}:${IMAGE_TAG} .

login:
	${CONTAINER_ENGINE} login -u ${CI_REGISTRY_USER} \
	-p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

publish: build login
	${CONTAINER_ENGINE} push ${IMAGE_NAME}:${IMAGE_TAG}
